package ru.rencredit.jschool.kuzyushin.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;

@Component
public abstract class AbstractListener {

    @NotNull
    public abstract String name();

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String description();

    public abstract void handler(final ConsoleEvent event) throws Exception;
}
