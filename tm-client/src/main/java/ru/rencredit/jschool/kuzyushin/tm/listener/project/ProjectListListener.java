package ru.rencredit.jschool.kuzyushin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;

import java.util.List;

@Component
public final class ProjectListListener extends AbstractListener {

    @NotNull
    private final ProjectSoapEndpoint projectSoapEndpoint;

    @Autowired
    public ProjectListListener(
            final @NotNull ProjectSoapEndpoint projectSoapEndpoint
    ) {
        this.projectSoapEndpoint = projectSoapEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    @EventListener(condition = "@projectListListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LIST PROJECTS]");
        @NotNull final List<ProjectDTO> projectsDTO = projectSoapEndpoint.findAllProjects();
        int index = 1;
        for (ProjectDTO projectDTO: projectsDTO) {
            System.out.println(index + ". " + projectDTO.getName());
            index++;
        }
        System.out.println("[OK]");
    }
}
