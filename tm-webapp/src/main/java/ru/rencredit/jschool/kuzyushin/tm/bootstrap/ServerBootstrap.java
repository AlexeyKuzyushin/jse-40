package ru.rencredit.jschool.kuzyushin.tm.bootstrap;

import org.springframework.stereotype.Component;

@Component
public final class ServerBootstrap {

    public void run(){
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }
}
