package ru.rencredit.jschool.kuzyushin.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ITaskService;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class TaskSoapEndpoint {

    @Autowired
    private ITaskService taskService;

    @NotNull
    @WebMethod
    public Long countAllTasks() {
        return taskService.count();
    }

    @NotNull
    @WebMethod
    public List<TaskDTO> findAllTasks() {
        return taskService.findAll();
    }

    @Nullable
    @WebMethod
    public TaskDTO findTaskById(@WebParam(name = "id", partName = "id") @Nullable final String id) {
        return TaskDTO.toDTO(taskService.findTaskById(id));
    }

    @WebMethod
    public void createTask(@WebParam(name = "taskDTO", partName = "taskDTO") @Nullable final TaskDTO taskDTO) {
        taskService.create(taskDTO.getUserId(), taskDTO.getName(), taskDTO.getProjectId(), taskDTO.getDescription());
    }

    @WebMethod
    public void updateTaskById(
            @WebParam(name = "taskDTO", partName = "taskDTO") @Nullable final TaskDTO taskDTO) {
            taskService.updateTaskById(taskDTO.getId(), taskDTO.getName(), taskDTO.getDescription());
    }

    @WebMethod
    public void removeTaskById(@WebParam(name = "id", partName = "id") @Nullable final String id) {
        taskService.removeTaskById(id);
    }
}
