<jsp:include page="../include/_header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<h1>TASK CREATE</h1>

<form:form action="/tasks/create" method="POST" modelAttribute="task">
    <p>
        <div style="margin-bottom: 5px">USER</div>
        <select name="userId">
            <c:forEach items="${users}" var="user">
                <option value=<c:out value="${user.id}"/>><c:out value="${user.login}"/></option>
            </c:forEach>
        </select>
    </p>
    <p>
        <div style="margin-bottom: 5px">PROJECT</div>
        <select name="projectId">
            <c:forEach items="${projects}" var="project">
                <option value=<c:out value="${project.id}"/>><c:out value="${project.name}"/></option>
            </c:forEach>
        </select>
    </p>
    <p>
        <div style="margin-bottom: 5px">NAME</div>
        <div><input type="text" name="name" value="${task.name}"></div>
    </p>
    <p>
        <div style="margin-bottom: 5px">DESCRIPTION</div>
        <div><input type="text" name="description" value="${task.description}"></div>
    </p>
    <button type="submit">SAVE PROJECT</button>
</form:form>

<jsp:include page="../include/_footer.jsp" />